package com.example.core.datasource;

import com.example.util.YamlPropertySourceFactory;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ConfigurationProperties(prefix = "spring.datasource")
@PropertySource(value = "classpath:application.yml", encoding = "utf-8", factory = YamlPropertySourceFactory.class)
public class HikariDataSourceProperties {

    private HikariDataSource hikari;

    public HikariDataSourceProperties() {
    }

    public HikariDataSource getHikari() {
        return this.hikari;
    }

    public void setHikari(HikariDataSource hikari) {
        this.hikari = hikari;
    }

}
