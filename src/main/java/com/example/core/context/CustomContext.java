package com.example.core.context;

public class CustomContext {

    private String app;

    private String dataSourceApp;

    public CustomContext(String app) {
        this.app = app;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getDataSourceApp() {
        return dataSourceApp;
    }

    public void setDataSourceApp(String dataSourceApp) {
        this.dataSourceApp = dataSourceApp;
    }
}
