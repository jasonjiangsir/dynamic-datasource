package com.example.core.context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommonContextInit {
    private static Logger logger = LoggerFactory.getLogger(CommonContextInit.class);

    public CommonContextInit() {
    }

    public static void init(CustomContext context) {
        initFinal(context);
    }

    private static void initFinal(CustomContext context) {
        CommonContext.initContext(context);
    }
}
