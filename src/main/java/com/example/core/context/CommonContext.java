package com.example.core.context;

import org.springframework.util.ObjectUtils;

public class CommonContext {

    private static final ThreadLocal<CommonContext> commonContextThreadLocal = new ThreadLocal();
    private CustomContext customContext;

    private CommonContext() {
    }

    public static CommonContext getContextNotExistInit() {
        CommonContext commonContext = (CommonContext)commonContextThreadLocal.get();
        if (ObjectUtils.isEmpty(commonContext)) {
            commonContext = new CommonContext();
            commonContextThreadLocal.set(commonContext);
        }

        return commonContext;
    }

    public static CommonContext getContext() {
        return (CommonContext)commonContextThreadLocal.get();
    }

    public static void initContext(CustomContext context) {
        CommonContext commonContext = getContextNotExistInit();
        commonContext.setCustomContext(context);
        commonContextThreadLocal.set(commonContext);
    }

    public static void remove() {
        commonContextThreadLocal.remove();
    }

    public CustomContext getCustomContext() {
        return this.customContext;
    }

    public void setCustomContext(CustomContext customContext) {
        this.customContext = customContext;
    }

}
