package com.example.core.config;

import com.example.core.datasource.DynamicDataSource;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

@Configuration
@AutoConfigureBefore({DataSourceAutoConfiguration.class})
public class DynamicDataSourceConfiguration {

    @Bean(name = {"dataSource"})
    public DynamicDataSource dataSource() {
        DynamicDataSource dataSource = new DynamicDataSource();
        dataSource.setTargetDataSources(new HashMap<>());
        return dataSource;
    }
}
