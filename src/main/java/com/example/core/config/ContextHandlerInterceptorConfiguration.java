package com.example.core.config;

import com.example.core.interceptor.ContextHandlerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class ContextHandlerInterceptorConfiguration implements WebMvcConfigurer {
    public ContextHandlerInterceptorConfiguration() {
    }

    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(this.userContextInterceptor()).addPathPatterns("/**").excludePathPatterns("/error");
    }

    @Bean
    public ContextHandlerInterceptor userContextInterceptor() {
        return new ContextHandlerInterceptor();
    }
}
