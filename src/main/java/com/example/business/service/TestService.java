package com.example.business.service;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface TestService {

    void selectUser() throws JsonProcessingException;

    void selectSource1User() throws JsonProcessingException;

    void selectSource2User() throws JsonProcessingException;

    void insertUser();

}
