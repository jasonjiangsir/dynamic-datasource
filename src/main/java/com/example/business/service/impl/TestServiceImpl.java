package com.example.business.service.impl;

import com.example.business.controller.TestController;
import com.example.business.dao.TestDao;
import com.example.business.service.TestService;
import com.example.core.annotation.DDS;
import com.example.core.datasource.DynamicDataSourceUtil;
import com.example.util.BeanTool;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

@Service
public class TestServiceImpl implements TestService {

    private Logger logger = LoggerFactory.getLogger(TestController.class);

    @Autowired
    private TestDao testDao;

    @Override
    public void selectUser() throws JsonProcessingException {
        TestService testService = BeanTool.getBean(TestService.class);
        testService.selectSource1User();
        testService.selectSource2User();
    }

    @Override
    public void selectSource1User() throws JsonProcessingException {
        List<Map<String, Object>> maps = testDao.selectUser();
        ObjectMapper objectMapper = new ObjectMapper();
        logger.info("xxxxxxxxxxxxxselect1:{}", objectMapper.writeValueAsString(maps));
    }

    @DDS("datasource2")
    @Override
    public void selectSource2User() throws JsonProcessingException {
        List<Map<String, Object>> maps = testDao.selectUser();
        ObjectMapper objectMapper = new ObjectMapper();
        logger.info("xxxxxxxxxxxxxselect2:{}", objectMapper.writeValueAsString(maps));
    }

    @Override
    public void insertUser() {
        testDao.insertUser();
        logger.info("xxxxxxxxxxxxxinsert:");
    }
}
