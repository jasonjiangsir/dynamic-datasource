package com.example.business.controller;

import com.example.business.service.TestService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private TestService testService;

    @RequestMapping("/test/select")
    public void testSelect() throws JsonProcessingException {
        testService.selectUser();
    }

    @RequestMapping("/test/insert")
    public void testInsert() throws JsonProcessingException {
        testService.insertUser();
    }

}
