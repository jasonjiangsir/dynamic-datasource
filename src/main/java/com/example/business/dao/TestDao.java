package com.example.business.dao;

import java.util.List;
import java.util.Map;

public interface TestDao {

    List<Map<String, Object>> selectUser();

    void insertUser();

}
