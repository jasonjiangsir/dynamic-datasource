## 多数据源配置
```
spring:
  datasource:
    hikari:
      max-lifetime: 90000
      connection-timeout: 60000
      maximum-pool-size: 20
      minimum-idle: 0
app:
  datasource:
    default: 'datasource1'
    dynamic:
      datasource1:
        jdbcUrl: 'jdbc:mysql://xxx:xxx/xxx?autoReconnect=true&useUnicode=true&allowMultiQueries=true&characterEncoding=utf8'
        username: 'xxx'
        password: 'xxx'
        driverClassName: 'com.mysql.cj.jdbc.Driver'
      datasource2:
        jdbcUrl: 'jdbc:mysql://xxx:xxx/xxx?autoReconnect=true&useUnicode=true&allowMultiQueries=true&characterEncoding=utf8'
        username: 'xxx'
        password: 'xxx'
        driverClassName: 'com.mysql.cj.jdbc.Driver'
```
default 为默认数据源
dynamic下为多个数据源

## 多数据源切换
1、根据CustomContext中的dataSourceApp,在收到请求时切换

2、根据注解@DDS(value = "")切面在调用此方法时切换
注意：不能在类内调用，因为代理的问题，可以自己了解，
提供了BeanTool.getBean()，从springboot中拿代理实例

3、根据手动调用DynamicDataSourceUtil手动切换数据源

注意：注意事务问题，请自行处理

